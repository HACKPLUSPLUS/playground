<?php
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;

class BookmarkTable
{

	const JOIN_INNER = 'inner';
	const JOIN_OUTER = 'outer';
	const JOIN_LEFT = 'left';
	const JOIN_RIGHT = 'right';

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
				
		$resultSet = $this->tableGateway->select();
		return $resultSet;
		
	}
	
	public function getBookmarkbyConcept() {
		
		//MANY TO MANY
		$sqlSelect = $this->tableGateway->getSql()->select();
		//$sqlSelect->columns(array());
		$sqlSelect->join('album_concepts', 'album_concepts.album_id = bookmarks.id', array(), self::JOIN_INNER);
		$sqlSelect->join('concepts', 'concepts.id = album_concepts.concepts_id', array(), self::JOIN_INNER);
		$sqlSelect->where(array('concepts.name' => 'PHP'));
		$resultSet = $this->tableGateway->selectWith($sqlSelect);
		
		return $resultSet;
		
	}

	public function getBookmark($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}
	
	public function getBookmarkResultSet($id)
	{
		$id  = (int) $id;
		$resultSet = $this->tableGateway->select(array('id' => $id));
		return $resultSet;
	}

	public function saveBookmark(Bookmark $bookmark)
	{
		$data =  array(
				'link' => $bookmark->link,
				'title'  => $bookmark->title,
				'catagory'	=> $bookmark->catagory,
		);

		$id = (int) $bookmark->id;
		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getBookmark($id)) {
				$this->tableGateway->update($data, array('id' => $id));
			} else {
				throw new \Exception('Bookmark with id does not exist');
			}
		}
	}

	public function deleteBookmark($id)
	{
		$this->tableGateway->delete(array('id' => (int) $id));
	}
}