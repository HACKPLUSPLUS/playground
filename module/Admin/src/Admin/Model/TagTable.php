<?php
namespace Admin\Model;

use Zend\Db\TableGateway\TableGateway;

class TagTable
{

	const JOIN_INNER = 'inner';
	const JOIN_OUTER = 'outer';
	const JOIN_LEFT = 'left';
	const JOIN_RIGHT = 'right';

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
				
		$resultSet = $this->tableGateway->select();
		return $resultSet;
		
	}
	
	public function getBookmarkbyConcept() {
		
		//MANY TO MANY
		$sqlSelect = $this->tableGateway->getSql()->select();
		//$sqlSelect->columns(array());
		$sqlSelect->join('album_concepts', 'album_concepts.album_id = album.id', array(), self::JOIN_INNER);
		$sqlSelect->join('concepts', 'concepts.id = album_concepts.concepts_id', array(), self::JOIN_INNER);
		$resultSet = $this->tableGateway->selectWith($sqlSelect);
		
		return $resultSet;
		
	}

	public function getTag($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function saveTag(Tag $tag)
	{
		$data =  array(
				'title'  => $tag->title,
		);

		$id = (int) $tag->id;
		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getTag($id)) {
				$this->tableGateway->update($data, array('id' => $id));
			} else {
				throw new \Exception('Tag id does not exist');
			}
		}
	}

	public function deleteTag($id)
	{
		$this->tableGateway->delete(array('id' => (int) $id));
	}
}