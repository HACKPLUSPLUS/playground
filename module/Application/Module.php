<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Admin\Model\Bookmark;
use Admin\Model\BookmarkTable;
use Admin\Model\BookmarkTag;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Admin\Model\Tag;
use Admin\Model\TagTable;
use Admin\Model\Login;
use Admin\Model\LoginTable;

class Module implements
AutoloaderProviderInterface,
ConfigProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $eventManager->attach('route', array($this, 'loadConfiguration'), 2);
        
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
                
    }
    
    public function loadConfiguration(MvcEvent $e)
    {
    	$application   = $e->getApplication();
    	$sm            = $application->getServiceManager();
    	$sharedManager = $application->getEventManager()->getSharedManager();
    		
    	$router = $sm->get('router');
    	$request = $sm->get('request');
    		
    	$matchedRoute = $router->match($request);
    	if (null !== $matchedRoute) {
    		$sharedManager->attach('Zend\Mvc\Controller\AbstractActionController','dispatch',
    				function($e) use ($sm) {
    					$sm->get('ControllerPluginManager')->get('Myplugin')
    					->doAuthorization($e); //pass to the plugin...
    				},2
    		);
    	}
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
        
    public function getServiceConfig()
    {
    	return array(
    		'factories' => array(
    			'BookmarkTableGateway' => function ($sm) {
    				$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    				$resultSetPrototype = new ResultSet();
    				$resultSetPrototype->setArrayObjectPrototype(new Bookmark());
    				return new TableGateway('bookmarks', $dbAdapter, null, $resultSetPrototype);
    			},
    			'Admin\Model\BookmarkTable' =>  function($sm) {
    				$tableGateway = $sm->get('BookmarkTableGateway');
    				
    				$table = new BookmarkTable($tableGateway);
    				return $table;
    			},
    			'TagTableGateway' => function ($sm) {
    				$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    				$resultSetPrototype = new ResultSet();
    				$resultSetPrototype->setArrayObjectPrototype(new Tag());
    				return new TableGateway('tag', $dbAdapter, null, $resultSetPrototype);
    			},
    			'Admin\Model\TagTable' =>  function($sm) {
    				$tableGateway = $sm->get('TagTableGateway');
    				$table = new TagTable($tableGateway);
    				return $table;
    			},
    			'LoginTableGateway' => function ($sm) {
    				$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    				$resultSetPrototype = new ResultSet();
    				$resultSetPrototype->setArrayObjectPrototype(new Login());
    				return new TableGateway('users', $dbAdapter, null, $resultSetPrototype);
    			},
    			'Admin\Model\LoginTable' =>  function($sm) {
    				$tableGateway = $sm->get('LoginTableGateway');
    				$table = new LoginTable($tableGateway);
    				return $table;
    			},
    		),
    	);
    }
}