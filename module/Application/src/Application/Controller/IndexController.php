<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
	protected $tagTable;
	protected $bookmarkTable;

    public function indexAction()
    {
    	return new ViewModel(array(
    			'tags'		=> $this->getTagTable()->fetchAll(),
    			'bookmarks'	=> $this->getBookmarkTable()->fetchAll(),
    	));
    }
    
    private function getTagTable()
    {
    	if (!$this->tagTable) {
    		$sm = $this->getServiceLocator();
    		$this->tagTable = $sm->get('Admin\Model\TagTable');
    	}
    	return $this->tagTable;
    }
    
    private function getBookmarkTable()
    {
    	if (!$this->bookmarkTable) {
    		$sm = $this->getServiceLocator();
    		$this->bookmarkTable = $sm->get('Admin\Model\BookmarkTable');
    	}
    	return $this->bookmarkTable;
    }
}