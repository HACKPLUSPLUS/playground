-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 02, 2015 at 05:20 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zf2tutorial`
--
CREATE DATABASE IF NOT EXISTS `zf2tutorial` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `zf2tutorial`;

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(200) NOT NULL,
  `title` varchar(100) NOT NULL,
  `catagory` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `link`, `title`, `catagory`) VALUES
(1, 'https://samsonasik.wordpress.com/2013/11/06/zend-framework-2-zenddb-modelling-the-hard-way/', 'Zend Framework 2 : Zend\\Db Modelling – The hard way', 'php'),
(2, 'Adele', '21', NULL),
(3, 'Bruce  Springsteen', 'Wrecking Ball (Deluxe)', NULL),
(4, 'Lana  Del  Rey', 'Born  To  Die', NULL),
(5, 'Gotye', 'Making  Mirrors', NULL),
(6, 'lkjlkj', 'lkjlkj', 'linux'),
(7, 'lkkjjlkjjlkkjlk', 'lkjlkjlkkj', 'windows'),
(8, 'more', 'test', 'windows');

-- --------------------------------------------------------

--
-- Table structure for table `album_concepts`
--

CREATE TABLE IF NOT EXISTS `album_concepts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `concepts_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `album_id` (`album_id`,`concepts_id`),
  KEY `id` (`id`),
  KEY `album_id_2` (`album_id`),
  KEY `concepts_id` (`concepts_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `album_concepts`
--

INSERT INTO `album_concepts` (`id`, `album_id`, `concepts_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 1),
(4, 4, 2),
(5, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `concepts`
--

CREATE TABLE IF NOT EXISTS `concepts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `concepts`
--

INSERT INTO `concepts` (`id`, `name`) VALUES
(1, 'Many to Many'),
(2, 'PHP'),
(3, 'MySQL');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `title`) VALUES
(1, 'PHP'),
(2, 'MySQL'),
(3, 'Zend'),
(4, 'Linux'),
(5, 'JavaScript'),
(6, 'Design Patterns'),
(7, 'Python'),
(8, 'Phalcon'),
(9, 'Application Framework');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `album_concepts`
--
ALTER TABLE `album_concepts`
  ADD CONSTRAINT `album_concepts_ibfk_2` FOREIGN KEY (`concepts_id`) REFERENCES `concepts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `album_concepts_ibfk_1` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
