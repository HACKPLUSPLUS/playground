<?php
return array(
		//controllers services...
		'controllers' => array(
				'factories' => array(
						'Admin\Controller\Auth' =>
						'Admin\Factory\Controller\AuthControllerServiceFactory'
				),
				'invokables' => array(
						'Admin\Controller\Bookmark' =>
						'Admin\Controller\BookmarkController',
						'Admin\Controller\Tag' =>
						'Admin\Controller\TagController'
				),
		),
		 
		//register auth services...
		'service_manager' => array(
				'factories' => array(
						'AuthStorage' =>
						'Admin\Factory\Storage\AuthStorageFactory',
						'AuthService' =>
						'Admin\Factory\Storage\AuthenticationServiceFactory',
						//register AlbumTable and TrackTable here
						//we can do same as the docs or using abstract_factories to "automate" them
						
						'BookmarkTag' => function($sm) {
							$bookmarktable = $sm->get('Admin\Model\BookmarkTable');
							$tagtable = $sm->get('Admin\Model\TagTable');
						
							$mapper = new Admin\Model\BookmarkTag($bookmarktable, $tagtable);
						
							return $mapper;
						},
				),
		),
		/*'service_manager' => array(
				'abstract_factories' => array(
						'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
						'Zend\Log\LoggerAbstractServiceFactory',
				),
				'factories' => array(
						'AuthStorage' =>
						'Admin\Factory\Storage\AuthStorageFactory',
						'AuthService' =>
						'Admin\Factory\Storage\AuthenticationServiceFactory',
				),        'aliases' => array(
						'translator' => 'MvcTranslator',
				),
		),
		'controllers' => array(
				'factories' => array(
						'Admin\Controller\Auth' => 'Admin\Controller\AuthController',
						'Admin\Controller\Bookmark' => 'Admin\Controller\BookmarkController',
						'Admin\Controller\Tag' => 'Admin\Controller\TagController',
				),
		),*/
		'router' => array(
				'routes' => array(
						'auth' => array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/admin/login',
										'defaults' => array(
												'controller' => 'Admin\Controller\Auth',
												'action'     => 'index',
										),
								),
						),
						'bookmark' => array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/admin/bookmark[/:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Admin\Controller\Bookmark',
												'action'     => 'index',
										),
								),
						),
						'tag' => array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/admin/tag[/:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Admin\Controller\Tag',
												'action'     => 'index',
										),
								),
						),
				),
		),
		'view_manager' => array(
				'template_path_stack' => array(
						'admin' => __DIR__ . '/../view',
				),
		),
);