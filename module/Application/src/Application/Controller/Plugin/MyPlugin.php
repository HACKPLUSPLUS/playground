<?php
namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin,
Zend\Session\Container as SessionContainer,
Zend\Permissions\Acl\Acl,
Zend\Permissions\Acl\Role\GenericRole as Role,
Zend\Permissions\Acl\Resource\GenericResource as Resource;
 
class Myplugin extends AbstractPlugin
{
	protected $sesscontainer ;

	private function getSessContainer()
	{
		if (!$this->sesscontainer) {
			$this->sesscontainer = new SessionContainer('zftutorial');
		}
		return $this->sesscontainer;
	}
	 
	public function doAuthorization($e)
	{
		//setting ACL...
		$acl = new Acl();
		//add role ..
		$acl->addRole(new Role('anonymous'));
		$acl->addRole(new Role('user'),  'anonymous');
		$acl->addRole(new Role('admin'), 'user');
		 
		$acl->addResource(new Resource('Application-index'));
		$acl->addResource(new Resource('Application-bookmarks'));
		$acl->addResource(new Resource('Admin-auth'));
		$acl->addResource(new Resource('Admin-bookmark'));
		$acl->addResource(new Resource('Admin-tag'));
		
		$acl->allow('anonymous', 'Application-index');
		$acl->allow('anonymous', 'Application-bookmarks');
		$acl->allow('anonymous', 'Admin-auth');
		$acl->deny('anonymous', 'Admin-bookmark');
		$acl->deny('anonymous', 'Admin-tag');
		$acl->allow('admin', 'Admin-bookmark');
		$acl->allow('admin', 'Admin-tag');
				 
		$controllerClass = get_class($e->getTarget());
		$module = substr($controllerClass, 0, strpos($controllerClass, '\\'));
		$controllerName = $e->getRouteMatch()->getMatchedRouteName();
		$resource = $module . '-' . $controllerName;
		//var_dump('Controller : ' . $controllerName);
		//var_dump('Module : ' . $module);
		
		$user = \Zend\Json\Json::decode( $this->getController()->getServiceLocator()->get('AuthService')->getStorage()->getSessionManager()
				->getSaveHandler()->read($this->getController()->getServiceLocator()->get('AuthService')->getStorage()->getSessionId()), true);
		//var_dump($user['username']);
		//$role = (! $this->getSessContainer()->role ) ? 'anonymous' : $this->getSessContainer()->role;
		$role = (! $user['username'] ) ? 'anonymous' : $user['username'];
		//var_dump($role);
		if ( ! $acl->isAllowed($role, $resource)){
			$router = $e->getRouter();
			$url    = $router->assemble(array(), array('name' => 'auth'));

			$response = $e->getResponse();
			$response->setStatusCode(302);
			//redirect to login route...
			/* change with header('location: '.$url); if code below not working */
			$response->getHeaders()->addHeaderLine('Location', $url);
			$e->stopPropagation();
		}
		
	}
}