<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TagsController extends AbstractActionController
{
	
	protected $tagTable;
    
    public function indexAction() {
    	
    	//$this->getBookmarkTable();
    	
    	//$sm = $this->getServiceLocator();
    	//$bookmarkTagMapper = $sm->get('BookmarkTag');
    	//$results = $bookmarkTagMapper->findAll();
    	//return new ViewModel(array(
    			//'bookmarks' => $results,
    	//));
    	
    	$id = (int) $this->params()->fromRoute('id', 0);
    	if (!$id) {
	    	return new ViewModel(array(
	    		'tags' => $this->getTagTable()->fetchAll(),
	    	));
    	}
    	/*elseif ($tag) {
    		return new ViewModel(array(
    				'bookmarks' => $this->getBookmarkTable()->getBookmarkbyConcept(),
    		));
    	}*/
    	else {
    		
    		return new ViewModel(array(
    			'tags' => $this->getTagTable()->getTagResultSet($id),
    		));
    	}
    }
    
    public function getTagTable() {
    	if (!$this->tagTable) {
    		$sm = $this->getServiceLocator();
    		$this->tagTable = $sm->get('Admin\Model\TagTable');
    	}
    	return $this->tagTable;
    }
    
}