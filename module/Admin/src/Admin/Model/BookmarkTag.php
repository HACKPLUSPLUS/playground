<?php
namespace Admin\Model;
use Admin\Model\Bookmark;

class BookmarkTag extends Bookmark
{

	protected $bookmarks;
	protected $tags;
	
	public function __construct(BookmarkTable $bookmark, TagTable $tag)
	{
		$this->bookmarks = $bookmark;
		$this->tags = $tag;
	}
	
	public function setTags($tags)
	{
		$this->tags = $tags;
	}
	
	public function getTags()
	{
		return $this->tags;
	}
	
	public function findAll()
	{
		return $this->bookmarks->getBookmarkbyConcept();
		//MANY TO MANY
		//$sqlSelect = $this->bookmarks->getSql()->select();
		//$sqlSelect->columns(array());
		//$sqlSelect->join('album_concepts', 'album_concepts.album_id = album.id', array(), self::JOIN_INNER);
		//$sqlSelect->join('concepts', 'concepts.id = album_concepts.concepts_id', array(), self::JOIN_INNER);
		//$sqlSelect->where(array('concepts.name' => 'PHP'));
		//	$resultSet = $this->bookmarks->selectWith($sqlSelect);

	}
}