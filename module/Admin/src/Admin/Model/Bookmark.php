<?php
namespace Admin\Model;

// Add these import statements
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Bookmark
{
	public $id;
	public $link;
	public $title;
	public $catagory;
	//protected $tags;
	protected $inputFilter;

	public function exchangeArray($data)
	{
		$this->id			= (!empty($data['id'])) ? $data['id'] : null;
		$this->link			= (!empty($data['link'])) ? $data['link'] : null;
		$this->title		= (!empty($data['title'])) ? $data['title'] : null;
		$this->catagory		= (!empty($data['catagory'])) ? $data['catagory'] : null;
	}
	
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}

	//public function setTags($tags)
	//{
		//$this->tags = $tags;
	//}
	
	//public function getTags()
	//{
		//return $this->tags;
	//}
	
	// Add content to these methods:
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}
	
	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();
	
			$inputFilter->add(array(
					'name'     => 'id',
					'required' => true,
					'filters'  => array(
							array('name' => 'Int'),
					),
			));
	
			$inputFilter->add(array(
					'name'     => 'link',
					'required' => true,
					'allow_empty' => false,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 200,
									),
							),
					),
			));
	
			$inputFilter->add(array(
					'name'     => 'title',
					'required' => true,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			));
			
			$inputFilter->add(array(
				'name'	=> 'catagory',
				'required'	=> false,
				'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
				),
			));
			
			$inputFilter->add(array(
					'name'	=> 'technology',
					'required'	=> false,
			));
			
			$inputFilter->add(array(
					'name'	=> 'concepts',
					'required'	=> false,
			));
	
			$this->inputFilter = $inputFilter;
		}
	
		return $this->inputFilter;
	}
}