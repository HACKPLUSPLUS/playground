<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Admin\Model\Bookmark;
use Admin\Form\BookmarkForm;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class BookmarkController extends AbstractActionController
{
	protected $bookmarkTable;
	
	public function indexAction() {
     	
     	// create a log channel
     	//$log = new Logger('name');
     	//$log->pushHandler(new StreamHandler('logs/app.log', Logger::WARNING));
     	
     	// add records to the log
     	//$log->addWarning('Foo');
     	//$log->addError('Bar');
     	
        return new ViewModel(array(
             'bookmarks' => $this->getBookmarkTable()->fetchAll(),
         ));
     }

	public function addAction()
	{
		$form = new BookmarkForm();
		$form->get('submit')->setValue('Add');
		
		$request = $this->getRequest();
		if ($request->isPost()) {
			$bookmark = new Bookmark();
			$form->setInputFilter($bookmark->getInputFilter());
			$form->setData($request->getPost());
		
			if ($form->isValid()) {
				$bookmark->exchangeArray($form->getData());
				$this->getBookmarkTable()->saveBookmark($bookmark);
		
				// Redirect to list of bookmarks
				return $this->redirect()->toRoute('bookmark');
			}
		}
		return array('form' => $form);
	}

	public function editAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('bookmark', array(
					'action' => 'add'
			));
		}
		
		// Get the Bookmark with the specified id.  An exception is thrown
		// if it cannot be found, in which case go to the index page.
		try {
			$bookmark = $this->getBookmarkTable()->getBookmark($id);
		}
		catch (\Exception $ex) {
			return $this->redirect()->toRoute('bookmark', array(
					'action' => 'index'
			));
		}
		
		$form  = new BookmarkForm();
		$form->bind($bookmark);
		$form->get('submit')->setAttribute('value', 'Edit');
		
		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->setInputFilter($bookmark->getInputFilter());
			$form->setData($request->getPost());
		
			if ($form->isValid()) {
				$this->getBookmarkTable()->saveBookmark($bookmark);
		
				// Redirect to list of bookmarks
				return $this->redirect()->toRoute('bookmark');
			}
		}
		
		return array(
				'id' => $id,
				'form' => $form,
		);
	}

	public function deleteAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('bookmark');
		}
		
		$request = $this->getRequest();
		if ($request->isPost()) {
			$del = $request->getPost('del', 'No');
		
			if ($del == 'Yes') {
				$id = (int) $request->getPost('id');
				$this->getBookmarkTable()->deleteBookmark($id);
			}
		
			// Redirect to list of bookmarks
			return $this->redirect()->toRoute('bookmark');
		}
		
		return array(
				'id'    => $id,
				'bookmark' => $this->getBookmarkTable()->getBookmark($id)
		);
	}
	
	public function getBookmarkTable() {
		if (!$this->bookmarkTable) {
			$sm = $this->getServiceLocator();
			$this->bookmarkTable = $sm->get('Admin\Model\BookmarkTable');
		}
		return $this->bookmarkTable;
	}
}