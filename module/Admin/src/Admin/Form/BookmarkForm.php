<?php
namespace Admin\Form;

use Zend\Form\Form;

class BookmarkForm extends Form
{
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('bookmark');

		$this->add(array(
				'name' => 'id',
				'type' => 'Hidden',
		));
		$this->add(array(
				'name' => 'title',
				'type' => 'Text',
				'options' => array(
						'label' => 'Title',
				),
				'attributes'	=> array(
						'required' => 'required',
				),
		));
		$this->add(array(
				'name' => 'link',
				'type' => 'Text',
				'options' => array(
						'label' => 'Link',
				),				
		));
		$this->add(array(
				'name' => 'catagory',
				'type' => 'Select',
				'attributes' => array(
						'id' => 'cat',
						'options' => array(
								'' => 'Choose',
								'linux' => 'Linux',
								'windows' => 'Windows',
								'php' => 'PHP',
								'tutorial' => 'Tutorial',
						),
				),
				'options' => array(
						'label' => 'Catagory',
				)
		
		));
		$this->add(array(
				'type' => 'Checkbox',
				'name' => 'technology',
				//'required' => false,
				//'allow_empty' => true,
				//'continue_if_empty' => true,
				'options' => array(
						'label' => 'Windows',
						//'use_hidden_element' => true,
						'checked_value' => 1,
						'unchecked_value' => 0
				),
				'attributes' => array(
						'value' => 1,
						//'allow_empty' => false,
				),
		));
		$this->add(
			array(
				'type' => 'MultiCheckbox',
				'name' => 'concepts',
				'options' => array(
					'label' => 'Concepts',
					'value_options' => array(
               			array(
                   			'value' => 'apple',
                   			'label' => 'Apple',
                   			'selected' => false,
                   			'disabled' => false,
                   			'attributes' => array(
                   				'id' => 'apple_option',
                   				'data-fruit' => 'apple',
                   			),
                   			'label_attributes' => array(
                      			'id' => 'apple_label',
                   			),
               			),
               			array(
                   			'value' => 'orange',
                   			'label' => 'Orange',
                   			'selected' => true,
               			),
               			array(
                   			'value' => 'lemon',
                   			'label' => 'Lemon',
               			),
					)
				)
            )
        );
		$this->add(array(
				'name' => 'submit',
				'type' => 'Submit',
				'attributes' => array(
						'value' => 'Go',
						'id' => 'submitbutton',
				),
		));
	}
}