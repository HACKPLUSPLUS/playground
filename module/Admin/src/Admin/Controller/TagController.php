<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Admin\Model\Tag;
use Admin\Form\TagForm;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class TagController extends AbstractActionController
{
	protected $tagTable;
	
	public function indexAction()
     {
     	     	
        return new ViewModel(array(
             'tags' => $this->getTagTable()->fetchAll(),
         ));
     }

	public function addAction()
	{
		$form = new TagForm();
		$form->get('submit')->setValue('Add');
		
		$request = $this->getRequest();
		if ($request->isPost()) {
			$tag = new Tag();
			$form->setInputFilter($tag->getInputFilter());
			$form->setData($request->getPost());
		
			if ($form->isValid()) {
				$tag->exchangeArray($form->getData());
				$this->getTagTable()->saveTag($tag);
		
				// Redirect to list of tags
				return $this->redirect()->toRoute('tag');
			}
		}
		return array('form' => $form);
	}

	public function editAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('tag', array(
					'action' => 'add'
			));
		}
		
		// Get the Tag with the specified id.  An exception is thrown
		// if it cannot be found, in which case go to the index page.
		try {
			$tag = $this->getTagTable()->getTag($id);
		}
		catch (\Exception $ex) {
			return $this->redirect()->toRoute('tag', array(
					'action' => 'index'
			));
		}
		
		$form  = new TagForm();
		$form->bind($tag);
		$form->get('submit')->setAttribute('value', 'Edit');
		
		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->setInputFilter($tag->getInputFilter());
			$form->setData($request->getPost());
		
			if ($form->isValid()) {
				$this->getTagTable()->saveTag($tag);
		
				// Redirect to list of tags
				return $this->redirect()->toRoute('tag');
			}
		}
		
		return array(
				'id' => $id,
				'form' => $form,
		);
	}

	public function deleteAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('tag');
		}
		
		$request = $this->getRequest();
		if ($request->isPost()) {
			$del = $request->getPost('del', 'No');
		
			if ($del == 'Yes') {
				$id = (int) $request->getPost('id');
				$this->getTagTable()->deleteTag($id);
			}
		
			// Redirect to list of tags
			return $this->redirect()->toRoute('tag');
		}
		
		return array(
				'id'    => $id,
				'tag' => $this->getTagTable()->getTag($id)
		);
	}
	public function getTagTable()
	{
		if (!$this->tagTable) {
			$sm = $this->getServiceLocator();
			$this->tagTable = $sm->get('Admin\Model\TagTable');
		}
		return $this->tagTable;
	}
}